import json

import pytest
from garbageBot import *
import responses


class TestGarbageBotSettings(object):
    @staticmethod
    def read_file(filename):
        with open(filename, 'r') as f:
            content = f.read()
        f.close()
        return content

    @pytest.fixture()
    def set_env_vars(self):
        os.environ['GL_SERVER_BASE'] = 'http://gitlab.int'
        os.environ['GL_AUTH_TOKEN'] = 'auth_token'
        os.environ['GL_BASE_PROJECT_ID'] = '42'
        os.environ['GL_BASE_PROJECT_PATH'] = 'namespace/project'

    @pytest.fixture()
    @pytest.mark.usefixtures('set_env_vars')
    def create_mock_server(self):
        html_file = self.read_file('gitlab-registry-garbage-bot/tests/res/raw_html.html')
        reg_info_json_dict = json.loads(self.read_file('gitlab-registry-garbage-bot/tests/res/reg_info.json'))
        branches_json_dict = json.loads(self.read_file('gitlab-registry-garbage-bot/tests/res/branchlist.json'))
        url_html = os.environ['GL_SERVER_BASE'] + '/' + os.environ['GL_BASE_PROJECT_PATH'] + '/container_registry'
        url_reg_info = url_html + '.json'
        url_branch_list = os.environ['GL_SERVER_BASE'] + '/api/v4/projects/' + os.environ['GL_BASE_PROJECT_ID'] + \
                          '/repository/branches'
        responses.add(responses.GET, url_html, body=html_file, status=200, adding_headers={
            'Set-Cookie': '_gitlab_session=session'
        })
        responses.add(responses.GET, url_reg_info, json=reg_info_json_dict, status=200)
        responses.add(responses.GET, url_branch_list, json=branches_json_dict, status=200)

    def test_transform_branch_ci_commit_ref_slug(self):
        branch_name = "feature/ISSUE-12345_My_Issue"
        assert GitlabServer.transform_branch_ci_commit_ref_slug(branch_name) == "feature-issue-12345-my-issue"

    def test_extract_csrf_token(self):
        with open('gitlab-registry-garbage-bot/tests/res/raw_html.html', 'r') as f:
            raw = f.read()
        f.close()
        assert GitlabServer.extract_csrf_token(raw) == "my_csrf_token"

    def test_read_settings_no_env(self):
        with pytest.raises(SystemExit) as exit_info:
            GitlabServer.read_settings()
        assert exit_info.value.code == 1

    @pytest.mark.usefixtures('set_env_vars')
    def test_read_settings_missing_server(self, capsys):
        del os.environ['GL_SERVER_BASE']
        with pytest.raises(SystemExit) as exit_info:
            GitlabServer.read_settings()
        cap = capsys.readouterr()
        assert cap.out == "Environment Variable 'GL_SERVER_BASE' not found. Exiting.\n"
        assert exit_info.value.code == 1

    @pytest.mark.usefixtures('set_env_vars')
    def test_read_settings_missing_token(self, capsys):
        del os.environ['GL_AUTH_TOKEN']
        with pytest.raises(SystemExit) as exit_info:
            GitlabServer.read_settings()
        cap = capsys.readouterr()
        assert cap.out == "Environment Variable 'GL_AUTH_TOKEN' not found. Exiting.\n"
        assert exit_info.value.code == 1

    @pytest.mark.usefixtures('set_env_vars')
    def test_read_settings_missing_project_id(self, capsys):
        del os.environ['GL_BASE_PROJECT_ID']
        with pytest.raises(SystemExit) as exit_info:
            GitlabServer.read_settings()
        cap = capsys.readouterr()
        assert cap.out == "Environment Variable 'GL_BASE_PROJECT_ID' not found. Exiting.\n"
        assert exit_info.value.code == 1

    @pytest.mark.usefixtures('set_env_vars')
    def test_read_settings_missing_project_path(self, capsys):
        del os.environ['GL_BASE_PROJECT_PATH']
        with pytest.raises(SystemExit) as exit_info:
            GitlabServer.read_settings()
        cap = capsys.readouterr()
        assert cap.out == "Environment Variable 'GL_BASE_PROJECT_PATH' not found. Exiting.\n"
        assert exit_info.value.code == 1

    @pytest.mark.usefixtures('set_env_vars')
    def test_read_settings_default(self):
        settings = GitlabServer.read_settings()
        assert len(settings) == 8
        assert settings['gl_server_base'] == 'http://gitlab.int'
        assert settings['gl_auth_token'] == 'auth_token'
        assert settings['gl_base_project_id'] == '42'
        assert settings['gl_base_project_path'] == 'namespace/project'
        assert settings['gl_keep_branch_images']
        assert not settings['purge_all']
        assert not settings['dry_run']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        cap = capsys.readouterr()
        print(cap.out)
        assert cap.out == "'GL_SCOPE_BY_REGISTRY' not set, defaulting to false " \
                          "Dry Run set to true, nothing should be deleted, only what would be deleted will be " \
                          "shown in the output\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond " \
                          "to a branch will be kept\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which " \
                          "images are deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['dry_run']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run_purge_all(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        os.environ['GL_PURGE_ALL'] = "True"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        del os.environ['GL_PURGE_ALL']
        cap = capsys.readouterr()
        assert cap.out == "'GL_SCOPE_BY_REGISTRY' not set, defaulting to false" \
                          'Dry Run set to true, nothing should be deleted, only what would be deleted will be ' \
                          'shown in the output\n' \
                          'Purge all set to true.  ' \
                          'All images will be deleted and all other options set will be ignored!\n'
        assert settings['dry_run']
        assert settings['purge_all']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_wo_dry_run_purge_all(self, capsys):
        os.environ['GL_PURGE_ALL'] = "True"
        settings = GitlabServer.read_settings()
        del os.environ['GL_PURGE_ALL']
        cap = capsys.readouterr()
        assert cap.out == "'GL_DRY_RUN' not set, defaulting to false\nPurge all set to true.  " \
                          "All images will be deleted and all other options set will be ignored!\n"
        assert settings['purge_all']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run_keep_branch_images(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        os.environ['GL_KEEP_BRANCH_IMAGES'] = "True"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        del os.environ['GL_KEEP_BRANCH_IMAGES']
        cap = capsys.readouterr()
        assert cap.out == "Dry Run set to true, nothing should be deleted, only what would be deleted will be shown " \
                          "in the output\n'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_IMAGES_TO_DELETE' " \
                          "not set, using existing branches to determine which images are deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['dry_run']
        assert settings['gl_keep_branch_images']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_wo_dry_run_keep_branch_images(self, capsys):
        os.environ['GL_KEEP_BRANCH_IMAGES'] = "True"
        settings = GitlabServer.read_settings()
        del os.environ['GL_KEEP_BRANCH_IMAGES']
        cap = capsys.readouterr()
        assert cap.out == "'GL_DRY_RUN' not set, defaulting to false\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which " \
                          "images are deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['gl_keep_branch_images']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run_keep_specified_images(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        os.environ['GL_IMAGES_TO_KEEP'] = "image1, image2"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        del os.environ['GL_IMAGES_TO_KEEP']
        cap = capsys.readouterr()
        assert cap.out == "Dry Run set to true, nothing should be deleted, only what would be deleted will be shown in the output\n'" \
                          "GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond to a branch will be kept\n'" \
                          "GL_IMAGES_TO_DELETE' not set, using existing branches to determine which images are deleted.\n" \
                          "Images specified for retention, the images specified will not be deleted\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['dry_run']
        assert settings['gl_images_to_keep']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_wo_dry_run_keep_specified_images(self, capsys):
        os.environ['GL_IMAGES_TO_KEEP'] = "image1, image2"
        settings = GitlabServer.read_settings()
        del os.environ['GL_IMAGES_TO_KEEP']
        cap = capsys.readouterr()
        print(cap)
        assert cap.out == "'GL_DRY_RUN' not set, defaulting to false\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond to a branch will be kept\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which images are deleted.\n" \
                          "Images specified for retention, the images specified will not be deleted\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['gl_keep_branch_images']

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run_list_delete(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        os.environ['GL_IMAGES_TO_DELETE'] = "testing1,testing2"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        del os.environ['GL_IMAGES_TO_DELETE']
        cap = capsys.readouterr()
        assert cap.out == "Dry Run set to true, nothing should be deleted, only what would be deleted will be shown " \
                          "in the output\n'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond to a " \
                          "branch will be kept\nImages specified for deletion, the images specified will be deleted\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['dry_run']
        assert settings['gl_images_to_delete'] == ["testing1", "testing2"]

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_wo_dry_run_list_delete(self, capsys):
        os.environ['GL_IMAGES_TO_DELETE'] = "testing1,testing2"
        settings = GitlabServer.read_settings()
        del os.environ['GL_IMAGES_TO_DELETE']
        cap = capsys.readouterr()
        assert cap.out == "'GL_DRY_RUN' not set, defaulting to false\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond" \
                          " to a branch will be kept\n" \
                          "Images specified for deletion, the images specified will be deleted\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['gl_images_to_delete'] == ["testing1", "testing2"]

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run_delete_older(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        os.environ['GL_DELETE_OLDER_THAN'] = "5"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        del os.environ['GL_DELETE_OLDER_THAN']
        cap = capsys.readouterr()
        assert cap.out == "Dry Run set to true, nothing should be deleted, only what would be deleted will" \
                          " be shown in the output\n'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. " \
                          "All images that correspond to a branch will be kept\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which " \
                          "images are deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "Older than setting has been set, all images older than 5 days will be deleted.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['dry_run']
        assert settings['gl_delete_older_than'] == "5"

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_wo_dry_run_delete_older(self, capsys):
        os.environ['GL_DELETE_OLDER_THAN'] = "5"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DELETE_OLDER_THAN']
        cap = capsys.readouterr()
        assert cap.out == "'GL_DRY_RUN' not set, defaulting to false\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond to a " \
                          "branch will be kept\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which images are" \
                          " deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "Older than setting has been set, all images older than 5 days will be deleted.\n" \
                          "'GL_KEEP_LATEST_VERSIONS' not set, latest versions will not be kept.\n"
        assert settings['gl_delete_older_than'] == "5"

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_w_dry_run_keep_latest(self, capsys):
        os.environ['GL_DRY_RUN'] = "True"
        os.environ['GL_KEEP_LATEST_VERSIONS'] = "5"
        settings = GitlabServer.read_settings()
        del os.environ['GL_DRY_RUN']
        del os.environ['GL_KEEP_LATEST_VERSIONS']
        cap = capsys.readouterr()
        assert cap.out == "Dry Run set to true, nothing should be deleted, only what would be deleted " \
                          "will be shown in the output\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond" \
                          " to a branch will be kept\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which images" \
                          " are deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "Keep latest versions has been set.  The last 5 versions of your image will not be deleted\n"
        assert settings['dry_run']
        assert settings['gl_keep_latest_versions'] == "5"

    @pytest.mark.usefixtures('set_env_vars')
    @pytest.mark.skip(reason="we need a new concept for these tests, skipping for now")
    def test_read_settings_wo_dry_run_keep_latest(self, capsys):
        os.environ['GL_KEEP_LATEST_VERSIONS'] = "5"
        settings = GitlabServer.read_settings()
        del os.environ['GL_KEEP_LATEST_VERSIONS']
        cap = capsys.readouterr()
        assert cap.out == "'GL_DRY_RUN' not set, defaulting to false\n" \
                          "'GL_PURGE_ALL' not set, defaulting to false\n" \
                          "'GL_KEEP_BRANCH_IMAGES' not set, defaulting to true. All images that correspond" \
                          " to a branch will be kept\n" \
                          "'GL_IMAGES_TO_DELETE' not set, using existing branches to determine which images " \
                          "are deleted.\n" \
                          "'GL_IMAGES_TO_KEEP' not set, no images are specified for retention.\n" \
                          "'GL_DELETE_OLDER_THAN' not set, no expiration date will be used.\n" \
                          "Keep latest versions has been set.  The last 5 versions of your image will not be deleted\n"
        assert settings['gl_keep_latest_versions'] == "5"

    @pytest.mark.usefixtures('create_mock_server')
    @responses.activate
    def test_get_response_csrf_session(self):
        gls = GitlabServer()
        assert gls.csrf_token == 'my_csrf_token'
        assert gls.sess_cookie == {'_gitlab_session': 'session'}

    @pytest.mark.usefixtures('create_mock_server')
    @responses.activate
    def test_get_registry_info(self):
        gls = GitlabServer()
        registry_info = gls.get_registry_info()
        assert registry_info['id'] == 42
        assert registry_info['path'] == "namespace/project/image"
        assert registry_info['location'] == "gitlab.int/namespace/project/image"
        assert registry_info['tags_path'] == "/namespace/project/registry/repository/42/tags?format=json"
        assert registry_info['destroy_path'] == "/namespace/project/container_registry/42.json"
