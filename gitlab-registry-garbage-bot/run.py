import sys

from GarbageBotMK2 import GitlabServer

if __name__ == '__main__':
    gls = GitlabServer()
    registry_repos = gls.get_registry_repositories_for_project()
    tag_name_list = []
    branch_slug_list = []
    image_tags_to_delete = []
    tag_list = []
    resp = None

    if gls.settings['dry_run']:
        print('#' * 10 + ' DRY RUN ' + '#' * 10)

    for reg in registry_repos:
        if gls.settings['gl_scope_registry']:
            tag_name_list.append(reg['name'])
        else:
            tag_list += gls.get_image_tags_for_registry_repository(reg['id'])
            for tag in tag_list:
                tag_name_list.append(tag['name'])
                tag['reg_id'] = reg['id']

    branches = gls.get_project_branches()
    for branch in branches:
        branch_slug_list.append(gls.normalize_branch_name(branch.attributes['name']))

    if gls.settings['gl_add_suffix_to_branch']:
        branch_slug_list_suf = []
        for branch in branch_slug_list:
            branch_slug_list_suf.append(branch + "-" + gls.settings['gl_add_suffix_to_branch'])
        branch_slug_list += branch_slug_list_suf

    if gls.settings['gl_project_environments']:
        if not int(gls.settings['gl_keep_latest_version']):
            raise Exception('You must specify GL_KEEP_LATEST_VERSIONS and it must be an integer')

        project_environments = set(gls.settings['gl_project_environments'].split(','))
        environments = gls.get_project_environments()
        deployments = gls.get_project_deployments(as_list=False, order_by='created_at', sort='desc')

        maximum_environment_images = int(gls.settings['gl_keep_latest_version'])
        environment_images = {}

        for deployment in deployments:
            for environment in environments:
                if environment.name in project_environments:
                    if not environment.name in environment_images:
                        environment_images[environment.name] = {
                            'keep': [],
                            'remove': [],
                        }

                    if deployment.environment['name'] == environment.name:
                        deployment_sha = deployment.sha[0:8]

                        if len(environment_images[environment.name]["keep"]) < maximum_environment_images:
                            # an image may have been redeployed to an environment
                            if not deployment_sha in environment_images[environment.name]["keep"]:
                                environment_images[environment.name]["keep"].append(deployment_sha)
                        elif len(environment_images[environment.name]["keep"]) >= maximum_environment_images:
                            # an image may have been redeployed to an environment
                            if not deployment_sha in environment_images[environment.name]["remove"]:
                                environment_images[environment.name]["remove"].append(deployment_sha)
                                image_tags_to_delete.append(deployment_sha)
    elif gls.settings['gl_keep_branch_images']:
        image_tags_to_delete = set(tag_name_list) - set(branch_slug_list)
    elif gls.settings['gl_delete_by_regex']:
        image_tags_to_delete = gls.filter_taglist_regex(gls.settings['gl_delete_by_regex'], tag_list)
    elif gls.settings['gl_keep_by_regex']:
        image_tags_to_delete = gls.filter_taglist_regex(gls.settings['gl_keep_by_regex'], tag_list, keep=True)
    elif gls.settings['gl_images_to_delete']:
        image_tags_to_delete = gls.settings['gl_images_to_delete'].split(',')
    elif gls.settings['gl_images_to_keep']:
        image_tags_to_delete = set(tag_name_list) - set(gls.settings['gl_images_to_keep'].split(','))
    elif gls.settings['gl_delete_older_than']:
        for reg in registry_repos:
            gls.response_handling_del_request(response=gls.delete_bulk_image_tag(registry_id=reg['id'],
                                                                                 older_than=
                                                                                 gls.settings['gl_delete_older_than']),
                                              reg_repo_name=reg['location'], scope='bulk')
    elif gls.settings['purge_all']:
        for reg in registry_repos:
            gls.response_handling_del_request(response=gls.delete_single_registry_repository(registry_id=reg['id']),
                                              reg_repo_name=reg['location'],
                                              scope='bulk')

    image_tags_to_delete = list(filter(None, image_tags_to_delete))

    for it in image_tags_to_delete:
        print('Deleting image {0}'.format(it))
        if gls.settings['gl_scope_registry']:
            for reg in registry_repos:
                if reg['name'] == it:
                    gls.response_handling_del_request(
                        response=gls.delete_single_registry_repository(registry_id=reg['id']),
                        image_tag=it)
        else:
            for tag in tag_list:
                if tag['name'] == it:
                    gls.response_handling_del_request(response=gls.delete_single_image_tag(registry_id=tag['reg_id'],
                                                                                           tag_name=it),
                                                      image_tag=it)
    print('Done')
