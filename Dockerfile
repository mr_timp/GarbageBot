FROM python:3-alpine
RUN apk update && apk upgrade && apk add --update alpine-sdk libxml2 libxml2-dev libxslt libxslt-dev
ADD . /GarbageBot
WORKDIR /GarbageBot/gitlab-registry-garbage-bot
RUN pip install -r ../requirements.txt
ENTRYPOINT ["python"]
CMD ["run.py"]